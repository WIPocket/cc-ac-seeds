-- Network ids:
id_monitor = "monitor_0";
id_chestO = "NBT_Observer_3";
id_o = {
  -- Closest to turtle
  "NBT_Observer_0",
  "NBT_Observer_1",
  "NBT_Observer_2"
};

local minFuelLevel = 100;
local minNumSticks = 4;
local expandTill = 16; -- Max 1 stack

-- Soil can not be replaced (would require a hoe and logic to not break sliktouchable grounds like mycelium)
local soil = {
  -- Closest to turtle
  "minecraft:farmland",
  "minecraft:farmland",
  "minecraft:farmland"
};

local underGround = {
  -- Closest to turtle
  "?",
  "?",
  "?"
};

local redstone_enabled = false;
local enabled = false;
local disabledReason;
local fuelLevel = 0;
local numSticks = 0;

local dontBreakUnderGround = { -- Don't break blocks that wouldnt drop
  "minecraft:lapis_ore",
  "minecraft:emerald_ore",
  "minecraft:glowstone",
  "minecraft:redstone_ore",
  "minecraft:quartz_ore",
  "minecraft:diamond_ore"
};

local gravityBlocks = { -- Don't break blocks under these
  "minecraft:sand"
};

local seedTypes = {};
function regSeed(shortName, info)
  local seed = {};
  seed.name = toFullSeedName(shortName);
  seed.soil = "minecraft:farmland";
  if info[1] then seed.soil = info[1]; end
  seed.underGround = "air";
  if info[2] then seed.underGround = info[2]; end
  seed.from = nil;
  if info[3] then seed.from = {toFullSeedName(info[3][1]), toFullSeedName(info[3][2])}; end
  seedTypes[seed.name] = seed;
end

function toFullSeedName(shortName)
  if not string.match(shortName, ":") then return "resource:" .. shortName .. "_plant" end
  if string.match(shortName, "^:") then return "vanilla" .. shortName .. "_plant" end
  return shortName;
end

-- regSeed(name, {soil | nil (=farmland), underGround | nil (=air), {name1, name2} | nil (=no crossbreeding)});
-- any plant name without ":" will be treated as "resource:$(name)_plant"
-- any plant name starting with ":" will be treated as "vanilla:$(name)_plant"
-- underGround format - "name/meta" (cuz thermal ores)

regSeed("minecraft:wheat_seeds", {});
regSeed("petinia", {nil, "thermalfoundation:ore/1", {"ferranium", "minecraft:wheat_seeds"}});
regSeed("ferranium", {nil, "minecraft:iron_ore/0", {"minecraft:melon_seeds", "minecraft:pumpkin_seeds"}});
regSeed("diamanhila", {nil, "minecraft:diamond_ore/0", {"aurigold", "nitorwart"}});
regSeed("plombean", {nil, "thermalfoundation:ore/3", {"ferranium", ":brown_mushroom"}});
regSeed("cuprosia", {nil, "thermalfoundation:ore/0", {"ferranium", ":carrot"}});
regSeed("platiolus", {nil, "thermalfoundation:ore/6", {"ferranium", "aurigold"}});
regSeed("lapender", {nil, "minecraft:lapis_ore/0", {"ferranium", ":potato"}});
regSeed("aurigold", {nil, "minecraft:gold_ore/0", {"ferranium", "redstodendron"}});
regSeed("emeryllis", {nil, "minecraft:emerald_ore/0", {"aurigold", "quartzanthemum"}});
regSeed("niccissus", {nil, "thermalfoundation:ore/5", {"ferranium", ":white_tulip"}});
regSeed("minecraft:pumpkin_seeds", {nil, nil, {":carrot", ":potato"}});
regSeed("minecraft:melon_seeds", {nil, nil, {":sugarcane", "minecraft:pumpkin_seeds"}});
regSeed("minecraft:beetroot_seeds", {});
regSeed(":carrot", {nil, nil, {"minecraft:wheat_seeds", ":potato"}});
regSeed(":daisy", {nil, nil, {":dandelion", ":orchid"}});
regSeed(":cactus", {"minecraft:sand", nil, {":sugarcane", ":potato"}});
regSeed(":allium", {nil, nil, {":poppy", ":orchid"}});
regSeed("quartzanthemum", {nil, "minecraft:quartz_ore/0", {"redstodendron", "nitorwart"}});
regSeed(":potato", {});
regSeed(":pink_tulip", {nil, nil, {":allium", ":dandelion"}});
regSeed(":orchid", {nil, nil, {":poppy", ":dandelion"}});
regSeed(":poppy", {nil, nil, {":sugarcane", "minecraft:melon_seeds"}});
regSeed(":nether_wart", {"minecraft:soul_sand"});
regSeed("nitorwart", {"minecraft:soul_sand", "minecraft:glowstone", {"redstodendron", ":carrot"}});
regSeed(":sugarcane", {"minecraft:sand", nil, {"minecraft:wheat_seeds", "minecraft:beetroot_seeds"}});
regSeed(":brown_mushroom", {"minecraft:mycelium", nil, {":potato", ":carrot"}});
regSeed("redstodendron", {nil, "minecraft:redstone_ore/0", {"ferranium", "lapender"}});
regSeed(":red_mushroom", {"minecraft:mycelium", nil, {":potato", ":carrot"}});
regSeed(":red_tulip", {nil, nil, {":poppy", ":allium"}});
regSeed(":dandelion", {nil, nil, {"minecraft:pumpkin_seeds", "minecraft:wheat_seeds"}});
regSeed(":white_tulip", {nil, nil, {":daisy", ":dandelion"}});
regSeed(":orange_tulip", {nil, nil, {":daisy", ":orchid"}});
regSeed("jaslumine", {nil, "thermalfoundation:ore/4", {"ferranium", ":potato"}});

-- Botania seeds:

function printState()
  mon = peripheral.wrap(id_monitor);
  mon.clear();
  mon.setCursorPos(1, 1);

  if redstoneenabled then
    mon.setTextColor(colors.green);
    mon.write("Redstone enabled ");
  else
    mon.setTextColor(colors.red);
    mon.write("Redstone disabled");
  end

  mon.setTextColor(colors.green);
  if fuelLevel < minFuelLevel then
    mon.setTextColor(colors.black);
    mon.setBackgroundColor(colors.red);
  end
  mon.write(" Fuel: " .. fuelLevel .. " ");

  mon.setTextColor(colors.green);
  mon.setBackgroundColor(colors.black);
  if numSticks < minNumSticks then
    mon.setTextColor(colors.black);
    mon.setBackgroundColor(colors.red);
  end
  mon.write(" Sticks: " .. numSticks .. " ");

  mon.setTextColor(colors.white);
  mon.setBackgroundColor(colors.black);
  mon.setCursorPos(1, 2);

  mon.write("Action: " .. textutils.serializeJSON(action));

  mon.setCursorPos(1, 4);
  mon.write("1| " .. printableNbt(nbt[1]));
  mon.setCursorPos(1, 5);
  mon.write("2| " .. printableNbt(nbt[2]));
  mon.setCursorPos(1, 6);
  mon.write("3| " .. printableNbt(nbt[3]));

  mon.setCursorPos(1, 8);
  mon.write("1| " .. underGround[1]);
  mon.setCursorPos(1, 9);
  mon.write("2| " .. underGround[2]);
  mon.setCursorPos(1, 10);
  mon.write("3| " .. underGround[3]);
end

function printableNbt(nbt)
  if not nbt then return ""; end
  if not nbt.agri_gain then
    if nbt.agri_cross_crop == 1 then return "Cross Sticks"; end
    return "Empty Sticks";
  end
  local str = "gr:" .. nbt.agri_growth .. " ga:" .. nbt.agri_gain .. " st:" .. nbt.agri_strength;
  str = str .. " s:" .. nbt.agri_seed .. " " .. math.floor(nbt.agri_meta/7*100) .. "%";
  return str;
end

local function has_value (tab, val)
  for index, value in ipairs(tab) do
    if value[1] == val then
      return true
    end
  end

  return false
end

function updateState()
  redstoneenabled = rs.getInput("left");
  fuelLevel = turtle.getFuelLevel();

  enabled = true;
  if not redstoneenabled then enabled = false; disabledReason = "No redstone signal"; end
  if numSticks < minNumSticks then enabled = false; disabledReason = "Not enough sticks"; end
  findItems();
end

seeds = {};
action = { type = "none" };

function findItems()
  seeds = {};
  numSticks = 0;

  items = peripheral.call(id_chestO, "read_nbt").Items;
  for k,v in pairs(items) do
    if v.id then
      if v.id == "agricraft:crop_sticks" then
        numSticks = numSticks + v.Count;
      end

      if (v.id == "agricraft:agri_seed")
        or (string.match(v.id, "^minecraft:%a+_seeds$")) then
        local seedName = v.id;
        if v.id == "agricraft:agri_seed" then
          seedName = v.tag.agri_seed;
        end
        local stat = getSeedStat(v);
        if isBetterStat(seeds[seedName], stat) then
          seeds[seedName] = {
            count = v.Count,
            stat = stat,
            pos = k
          };
        end
      end
    end
  end
end

function getSeedStat(item)
  if item.tag and item.tag.agri_growth then
    return {item.tag.agri_growth, item.tag.agri_gain, item.tag.agri_strength};
  else return {1, 1, 1}; end
end

function isBetterStat(old, new)
  if old == nil then return true; end
  if old.stat then return isBetterStat(old.stat, new); end

  if new[1] > old[1] or new[2] > old[2] or new[3] > old[3] then
    return true;
  else
    return false;
  end
end

function isFullStat(stat)
  if stat[1] == 10 and stat[2] == 10 and stat[3] == 10 then
    return true;
  else
    return false;
  end
end

nbt = {false, false, false};

function setUnderground(newUnderGround)
  if (underGround[1] == newUnderGround[1] or newUnderGround[1] == "air") and
     (underGround[2] == newUnderGround[2] or newUnderGround[2] == "air") and
     (underGround[3] == newUnderGround[3] or newUnderGround[3] == "air") then
    return true;
  end

  if has_value(dontBreakUnderGround, newUnderGround[1]) or
     has_value(dontBreakUnderGround, newUnderGround[2]) or
     has_value(dontBreakUnderGround, newUnderGround[3]) then
    return false;
  end

  if has_value(gravityBlocks, soil[1]) or
     has_value(gravityBlocks, soil[2]) or
     has_value(gravityBlocks, soil[3]) then
    return false;
  end

  local chest = peripheral.wrap("bottom");
  local succ = true;
  if not (newUnderGround[1] == "air") then
    itemPos = findItem(newUnderGround[1], true);
    if itemPos == nil then
      print(newUnderGround[1]);
      succ = false;
    else
      chest.pushItems("up", itemPos, 1, 1);
    end
  end
  if not (newUnderGround[2] == "air") then
    itemPos = findItem(newUnderGround[2], true);
    if itemPos == nil then
      print(newUnderGround[2]);
      succ = false;
    else
      chest.pushItems("up", itemPos, 1, 2);
    end
  end
  if not (newUnderGround[3] == "air") then
    itemPos = findItem(newUnderGround[3], true);
    if itemPos == nil then
      print(newUnderGround[3]);
      succ = false;
    else
      chest.pushItems("up", itemPos, 1, 3);
    end
  end

  if succ then
    turtle.forward();
    turtle.forward();
    turtle.forward();
    turtle.forward();
    turtle.down();
    turtle.down();
    turtle.down();
    turtle.turnRight();
    turtle.turnRight();
    turtle.dig();
    turtle.forward();
    turtle.dig();
    turtle.forward();
    turtle.dig();

    turtle.select(1);
    if not (newUnderGround[1] == "air") then turtle.place(); end
    turtle.turnRight();
    turtle.turnRight();
    turtle.forward();
    turtle.turnRight();
    turtle.turnRight();

    turtle.select(2);
    if not (newUnderGround[2] == "air") then turtle.place(); end
    turtle.turnRight();
    turtle.turnRight();
    turtle.forward();
    turtle.turnRight();
    turtle.turnRight();

    turtle.select(3);
    if not (newUnderGround[3] == "air") then turtle.place(); end
    turtle.turnRight();
    turtle.turnRight();
    turtle.forward();

    turtle.up();
    turtle.up();
    turtle.up();
    turtle.turnRight();
    turtle.turnRight();
    turtle.forward();
    turtle.forward();
    turtle.forward();
    turtle.forward();
    turtle.turnRight();
    turtle.turnRight();

    underGround = newUnderGround;
  else
    print("something not found (setUnderground failed)");
  end

  for i=1,6 do
    turtle.select(i);
    turtle.dropDown();
  end
  turtle.select(1);

  return succ;
end

function checkSoil(arr)
  if arr[1] and not (arr[1] == soil[1]) then return false; end
  if arr[2] and not (arr[2] == soil[2]) then return false; end
  if arr[3] and not (arr[3] == soil[3]) then return false; end
  return true;
end

function chooseAction()
  if not enabled then return { type = "none", reason = disabledReason }; end

  if fuelLevel < minFuelLevel then return { type = "refuel" } end

  nbt = {false, false, false};
  for i=1,3 do
    local has_nbt = peripheral.call(id_o[i], "has_nbt");
    if has_nbt then nbt[i] = peripheral.call(id_o[i], "read_nbt"); end
  end

  if nbt[1] then -- something planted
    if nbt[3] then -- mutating or crossbreeding
      if nbt[1].agri_seed == nbt[3].agri_seed then -- mutating
        if nbt[2] and nbt[2].agri_seed then -- something grew
          if isBetterStat(
                          {nbt[1].agri_growth, nbt[1].agri_gain, nbt[1].agri_strength},
                          {nbt[2].agri_growth, nbt[2].agri_gain, nbt[2].agri_strength}) then
            return { type = "break", what = "all", reason = "better seed grew" };
          else
            return { type = "break", what = "mid", reason = "same seed grew" };
          end
        else
          return { type = "none", reason = "wait, mutating" };
        end
      else -- crossbreeding
        if nbt[2] and nbt[2].agri_seed then -- something grew
          return { type = "break", what = "all", reason = "new seed grew" };
        else
          return { type = "none", reason = "wait, crossbreeding" };
        end
      end
    else -- multipling
      if nbt[2] and nbt[2].agri_seed then -- something grew
        return { type = "break", what = "two", reason = "seed multiplied" };
      else
        return { type = "none", reason = "wait, multipling" };
      end
    end
  end

  for seedName,seedDef in pairs(seedTypes) do
    if not seeds[seedName] then -- skip seeds already present
      if seedDef.from then
        if checkSoil({seedTypes[seedDef.from[1]].soil, seedDef.soil, seedTypes[seedDef.from[2]].soil}) then
          if seeds[seedDef.from[1]] and seeds[seedDef.from[2]] then
            if setUnderground({
              seedTypes[seedDef.from[1]].underGround,
              seedDef.underGround,
              seedTypes[seedDef.from[2]].underGround}) then
                return { type = "crossbreed", s1 = seeds[seedDef.from[1]].pos, s2 = seeds[seedDef.from[2]].pos };
              end
            end
          end
        end
      end
    end

  for k,v in pairs(seeds) do
    if not isFullStat(v.stat) then
      if checkSoil({seedTypes[k].soil, seedTypes[k].soil, seedTypes[k].soil})
        and setUnderground({seedTypes[k].underGround, seedTypes[k].underGround, seedTypes[k].underGround}) then
        if v.count < 2 then
          return { type = "expand", reason = "not enough to mutate", slot = v.pos };
        end
        return { type = "mutate", reason = "not full", slot = v.pos };
      end
    end
  end

  for k,v in pairs(seeds) do
    if isFullStat(v.stat) then
      if v.count < expandTill then
        if checkSoil({seedTypes[k].soil, seedTypes[k].soil, nil}) and setUnderground({seedTypes[k].underGround, seedTypes[k].underGround, "air"}) then
          return { type = "expand", reason = v.count .. " < " .. expandTill, slot = v.pos };
        end
      end
    end
  end

  setUnderground({"air", "air", "air"});
  return { type = "none", reason = "no action" };
end

function doAction()
  local chest = peripheral.wrap("bottom");

  if action.type == "refuel" then
    turtle.select(16);
    while true do
      local succ = turtle.refuel(1);
      if turtle.getFuelLevel() > minFuelLevel then return; end
      if not succ then break; end
    end

    peripheral.call(id_monitor, "write", " No fuel!");
  end

  if action.type == "expand" then
    chest.pushItems("up", findStick(), 1);
    chest.pushItems("up", findStick(), 1);
    chest.pushItems("up", action.slot, 1);

    turtle.select(1);

    turtle.forward();
    turtle.placeDown();
    turtle.forward();
    turtle.placeDown();

    turtle.select(2);
    turtle.turnLeft();
    turtle.forward();
    turtle.turnLeft();
    turtle.forward();
    turtle.dropDown(1);
    rs.setOutput("bottom", true);
    sleep(0.1);
    rs.setOutput("bottom", false);
    turtle.turnLeft();
    turtle.forward();
    turtle.turnRight();
    turtle.forward();
    turtle.turnRight();
    turtle.turnRight();
  end

  if action.type == "mutate" then
    chest.pushItems("up", findStick(), 1);
    chest.pushItems("up", findStick(), 1);
    chest.pushItems("up", findStick(), 1);
    chest.pushItems("up", findStick(), 1);
    chest.pushItems("up", action.slot, 2);

    turtle.select(1);

    turtle.forward();
    turtle.placeDown();
    turtle.forward();
    turtle.placeDown();
    turtle.forward();
    turtle.placeDown();

    turtle.turnLeft();
    turtle.forward();
    turtle.turnLeft();

    turtle.select(2);
    turtle.dropDown(1);
    rs.setOutput("bottom", true);
    sleep(0.1);
    rs.setOutput("bottom", false);

    turtle.forward();

    turtle.select(1);
    turtle.dropDown(1);

    turtle.forward();

    turtle.select(2);
    turtle.dropDown(1);
    rs.setOutput("bottom", true);
    sleep(0.1);
    rs.setOutput("bottom", false);

    turtle.turnLeft();
    turtle.forward();
    turtle.turnRight();
    turtle.forward();
    turtle.turnRight();
    turtle.turnRight();
  end

  if action.type == "break" then
    if action.what == "two" then
      turtle.forward();
      turtle.forward();
      turtle.turnRight();
      turtle.turnRight();
      turtle.digDown();
      turtle.forward();
      turtle.digDown();
      turtle.forward();
      turtle.turnRight();
      turtle.turnRight();
    end

    if action.what == "mid" then
      chest.pushItems("up", findStick(), 1);

      turtle.forward();
      turtle.forward();
      turtle.digDown();
      turtle.select(1);
      turtle.placeDown();
      turtle.turnLeft();
      turtle.forward();
      turtle.dropDown(1);
      rs.setOutput("bottom", true);
      sleep(0.1);
      rs.setOutput("bottom", false);
      turtle.turnRight();
      turtle.turnRight();
      turtle.forward();

      turtle.turnRight();
      turtle.forward();
      turtle.forward();
      turtle.turnRight();
      turtle.turnRight();
    end

    if action.what == "all" then
      turtle.forward();
      turtle.digDown();
      turtle.forward();
      turtle.digDown();
      turtle.forward();
      turtle.digDown();

      turtle.turnRight();
      turtle.turnRight();
      turtle.forward();
      turtle.forward();
      turtle.forward();
      turtle.turnRight();
      turtle.turnRight();
    end

    for i=1,6 do
      turtle.select(i);
      turtle.dropDown();
    end
    turtle.select(1);
  end

  if action.type == "crossbreed" then
    chest.pushItems("up", action.s1, 1, 1);
    chest.pushItems("up", action.s2, 1, 2);
    chest.pushItems("up", findStick(), 1, 3);
    chest.pushItems("up", findStick(), 1, 3);
    chest.pushItems("up", findStick(), 1, 3);
    chest.pushItems("up", findStick(), 1, 3);

    turtle.select(3);

    turtle.forward();
    turtle.placeDown();
    turtle.forward();
    turtle.placeDown();
    turtle.forward();
    turtle.placeDown();

    turtle.turnLeft();
    turtle.forward();
    turtle.turnLeft();

    turtle.select(2);
    turtle.dropDown(1);
    rs.setOutput("bottom", true);
    sleep(0.1);
    rs.setOutput("bottom", false);

    turtle.forward();

    turtle.select(3);
    turtle.dropDown(1);

    turtle.forward();

    turtle.select(1);
    turtle.dropDown(1);
    rs.setOutput("bottom", true);
    sleep(0.1);
    rs.setOutput("bottom", false);

    turtle.turnLeft();
    turtle.forward();
    turtle.turnRight();
    turtle.forward();
    turtle.turnRight();
    turtle.turnRight();
  end
end

function findStick()
  return findItem("agricraft:crop_sticks", false);
end

function findItem (itemName, useMeta)
  items = peripheral.call(id_chestO, "read_nbt").Items;
  for k,v in pairs(items) do
    if v.id then
      local testName = v.id;
      if useMeta then testName = testName .. "/" .. v.Damage; end
      if testName == itemName then
        return k;
      end
    end
  end
end

while true do
  updateState();
  action = chooseAction();
  printState();
  doAction();
  sleep(1);
end
